# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Marco Camplese <marco.camplese.mc@gmail.com>, 2016-2017
# Roberto Rosario, 2016
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-10-29 12:43-0400\n"
"PO-Revision-Date: 2018-09-27 02:30+0000\n"
"Last-Translator: Marco Camplese <marco.camplese.mc@gmail.com>\n"
"Language-Team: Italian (http://www.transifex.com/rosarior/mayan-edms/language/it/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: it\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:115 apps.py:268 events.py:7 menus.py:10 models.py:238
#: permissions.py:7 queues.py:18 settings.py:12 statistics.py:231
msgid "Documents"
msgstr "Documenti"

#: apps.py:136
msgid "Create a document type"
msgstr "Creare un tipo di documento"

#: apps.py:138
msgid ""
"Every uploaded document must be assigned a document type, it is the basic "
"way Mayan EDMS categorizes documents."
msgstr "Ad ogni documento caricato deve essere assegnato un tipo documento,  questa è la via più semplice per categorizzare i documenti in Mayan EDMS."

#: apps.py:157
msgid "Versions comment"
msgstr ""

#: apps.py:160
msgid "Versions encoding"
msgstr ""

#: apps.py:163
msgid "Versions mime type"
msgstr ""

#: apps.py:166
msgid "Versions timestamp"
msgstr ""

#: apps.py:231 apps.py:248 apps.py:255 apps.py:283 apps.py:298 apps.py:324
msgid "Thumbnail"
msgstr "Miniatura"

#: apps.py:240 apps.py:307 forms.py:186 links.py:84
msgid "Pages"
msgstr "Pagine"

#: apps.py:262
msgid "Type"
msgstr "Tipo"

#: apps.py:275 models.py:769
msgid "Enabled"
msgstr "Abilitato"

#: apps.py:330 links.py:366 views/document_views.py:846
msgid "Duplicates"
msgstr "Duplicati"

#: dashboard_widgets.py:24
msgid "Total pages"
msgstr ""

#: dashboard_widgets.py:46
msgid "Total documents"
msgstr "Totale documenti"

#: dashboard_widgets.py:65 views/document_views.py:168
msgid "Documents in trash"
msgstr "Documenti nel cestino"

#: dashboard_widgets.py:84 links.py:352 links.py:357 permissions.py:55
#: views/document_type_views.py:71
msgid "Document types"
msgstr "Tipi di documento"

#: dashboard_widgets.py:103
msgid "New documents this month"
msgstr "Nuovi documenti in questo mese"

#: dashboard_widgets.py:116
msgid "New pages this month"
msgstr "Nuove pagine in questo mese"

#: events.py:10
msgid "Document created"
msgstr "Documento creato"

#: events.py:13
msgid "Document downloaded"
msgstr "Documenti scaricati"

#: events.py:16
msgid "New version uploaded"
msgstr "Nuove versioni caricate"

#: events.py:19
msgid "Document properties edited"
msgstr "Modificate proprietà del documento "

#: events.py:23
msgid "Document type changed"
msgstr "Cambiamenti al tipo di documento"

#: events.py:27
msgid "Document type created"
msgstr ""

#: events.py:31
msgid "Document type edited"
msgstr ""

#: events.py:34
msgid "Document version reverted"
msgstr "Versioni di documento recuperate"

#: events.py:37
msgid "Document viewed"
msgstr "Documenti visualizzati"

#: forms.py:96
msgid "Quick document rename"
msgstr "Rinomina del documento veloce"

#: forms.py:104 forms.py:256
msgid "Preserve extension"
msgstr ""

#: forms.py:106
msgid ""
"Takes the file extension and moves it to the end of the filename allowing "
"operating systems that rely on file extensions to open document correctly."
msgstr ""

#: forms.py:149
msgid "Date added"
msgstr "Data inserimento"

#: forms.py:153 models.py:185
msgid "UUID"
msgstr "UUID"

#: forms.py:155 models.py:209
msgid "Language"
msgstr "Lingua"

#: forms.py:157
msgid "Unknown"
msgstr "Sconosciuto"

#: forms.py:165
msgid "File mimetype"
msgstr "File mimetype"

#: forms.py:166 forms.py:171
msgid "None"
msgstr "Nessuna "

#: forms.py:169
msgid "File encoding"
msgstr "File encoding"

#: forms.py:175 models.py:1018
msgid "File size"
msgstr "Dimensioni del file"

#: forms.py:180
msgid "Exists in storage"
msgstr "Esiste nello storage"

#: forms.py:182
msgid "File path in storage"
msgstr "File path in storage"

#: forms.py:185 models.py:464 search.py:24 search.py:48
msgid "Checksum"
msgstr "Checksum"

#: forms.py:213 models.py:103 models.py:189 models.py:764 search.py:16
#: search.py:35
msgid "Document type"
msgstr "Tipo documento "

#: forms.py:229
msgid "Compress"
msgstr "Comprimere"

#: forms.py:231
msgid ""
"Download the document in the original format or in a compressed manner. This"
" option is selectable only when downloading one document, for multiple "
"documents, the bundle will always be downloads as a compressed file."
msgstr "Scarica il documento nel formato originale oppure compresso. Questa opzione è selezionabile quando scarichi un singolo documento, per documenti multipli, il pacchetto sarà sempre scaricato in formato compresso."

#: forms.py:238
msgid "Compressed filename"
msgstr "Nome file compresso "

#: forms.py:241
msgid ""
"The filename of the compressed file that will contain the documents to be "
"downloaded, if the previous option is selected."
msgstr "Il nome file del file compresso che conterrà il documento da scaricare, se l'opzione precedente è selezionata."

#: forms.py:258
msgid ""
"Takes the file extension and moves it to the end of the filename allowing "
"operating systems that rely on file extensions to open the downloaded "
"document version correctly."
msgstr ""

#: forms.py:270 literals.py:39
msgid "Page range"
msgstr "Intervallo pagina"

#: forms.py:276
msgid ""
"Page number from which all the transformation will be cloned. Existing "
"transformations will be lost."
msgstr "Numero di pagina da cui le trasformazioni saranno clonate. Le trasformazioni esistenti andranno perse."

#: links.py:70
msgid "Preview"
msgstr "Anteprima "

#: links.py:75
msgid "Properties"
msgstr "Proprietà"

#: links.py:80 links.py:200
msgid "Versions"
msgstr "Versioni"

#: links.py:92 links.py:152
msgid "Clear transformations"
msgstr "Cancella trasformazioni"

#: links.py:97
msgid "Clone transformations"
msgstr "Clona trasformazioni"

#: links.py:102 links.py:160 links.py:325 links.py:340
msgid "Delete"
msgstr "Cancella"

#: links.py:106 links.py:164
msgid "Add to favorites"
msgstr ""

#: links.py:111 links.py:168
msgid "Remove from favorites"
msgstr ""

#: links.py:116 links.py:156
msgid "Move to trash"
msgstr "Sposta nel cestino"

#: links.py:122
msgid "Edit properties"
msgstr "Modifica proprietà"

#: links.py:126 links.py:172
msgid "Change type"
msgstr "Cambia tipo"

#: links.py:131 links.py:176
msgid "Advanced download"
msgstr ""

#: links.py:135
msgid "Print"
msgstr "Stampa"

#: links.py:139
msgid "Quick download"
msgstr ""

#: links.py:143 links.py:179
msgid "Recalculate page count"
msgstr "Ricalcola numero pagine"

#: links.py:147 links.py:183
msgid "Restore"
msgstr "Ripristina"

#: links.py:189
msgid "Download version"
msgstr "Scarica versione"

#: links.py:194 links.py:275 models.py:237 models.py:427 models.py:1052
#: models.py:1082 models.py:1111
msgid "Document"
msgstr "Documento"

#: links.py:205
msgid "Details"
msgstr "Dettagli"

#: links.py:210 views/document_views.py:96
msgid "All documents"
msgstr "Tutti i documenti"

#: links.py:214 views/document_views.py:885
msgid "Favorites"
msgstr ""

#: links.py:218 views/document_views.py:969
msgid "Recently accessed"
msgstr ""

#: links.py:222 views/document_views.py:993
msgid "Recently added"
msgstr ""

#: links.py:226
msgid "Trash can"
msgstr "Cestino"

#: links.py:234
msgid ""
"Clear the graphics representations used to speed up the documents' display "
"and interactive transformations results."
msgstr "Cancella le rappresentazioni grafiche utilizzate per accellerare la visualizzazione dei documenti e dei risultati interattivi trasformazioni."

#: links.py:237
msgid "Clear document image cache"
msgstr "Pulisci la cache delle immagini dei documenti"

#: links.py:241 permissions.py:51
msgid "Empty trash"
msgstr "Svuota cestino"

#: links.py:250
msgid "First page"
msgstr "Prima pagina"

#: links.py:255
msgid "Last page"
msgstr "Ultima pagina"

#: links.py:263
msgid "Previous page"
msgstr "Pagina precedente"

#: links.py:269
msgid "Next page"
msgstr "Pagina successiva"

#: links.py:281
msgid "Rotate left"
msgstr "Ruota a sinistra"

#: links.py:286
msgid "Rotate right"
msgstr "Ruota a destra"

#: links.py:289
msgid "Page image"
msgstr "Immagine della pagina"

#: links.py:293
msgid "Reset view"
msgstr "Reset visualizzazione"

#: links.py:299
msgid "Zoom in"
msgstr "Zoom in"

#: links.py:305
msgid "Zoom out"
msgstr "Zoom out"

#: links.py:313
msgid "Revert"
msgstr "Ritornare"

#: links.py:320 views/document_type_views.py:86
msgid "Create document type"
msgstr "Crea tipo di documento"

#: links.py:329 links.py:345
msgid "Edit"
msgstr "Modifica"

#: links.py:335
msgid "Add quick label to document type"
msgstr "Aggiungi un'etichetta rapida al tipo documento"

#: links.py:349 models.py:775
msgid "Quick labels"
msgstr "Etichette rapide"

#: links.py:361 models.py:1055 models.py:1065 views/document_views.py:865
msgid "Duplicated documents"
msgstr ""

#: links.py:372
msgid "Duplicated document scan"
msgstr "Documento scansionato duplicato"

#: literals.py:30
msgid "Default"
msgstr "Default"

#: literals.py:39
msgid "All pages"
msgstr "Tutte le pagine"

#: models.py:74
msgid "The name of the document type."
msgstr ""

#: models.py:75 models.py:193 models.py:767 search.py:21 search.py:42
msgid "Label"
msgstr "Etichetta"

#: models.py:79
msgid ""
"Amount of time after which documents of this type will be moved to the "
"trash."
msgstr "Quantità di tempo dopo il quale i documenti di questo tipo saranno spostati nel cestino."

#: models.py:81
msgid "Trash time period"
msgstr "Tempo per cestinare"

#: models.py:85
msgid "Trash time unit"
msgstr "Unità di tempo per cestinare"

#: models.py:89
msgid ""
"Amount of time after which documents of this type in the trash will be "
"deleted."
msgstr "Quantità di tempo dopo il quale i documenti di questo tipo nel cestino saranno cancellati."

#: models.py:91
msgid "Delete time period"
msgstr "Tempo per cancellare"

#: models.py:96
msgid "Delete time unit"
msgstr "Unità di tempo per cancellare"

#: models.py:104
msgid "Documents types"
msgstr "Tipi di documenti"

#: models.py:183
msgid ""
"UUID of a document, universally Unique ID. An unique identifiergenerated for"
" each document."
msgstr ""

#: models.py:193
msgid "The name of the document."
msgstr ""

#: models.py:197
msgid "An optional short text describing a document."
msgstr ""

#: models.py:198 search.py:22 search.py:45
msgid "Description"
msgstr "Descrizione "

#: models.py:202
msgid ""
"The server date and time when the document was finally processed and added "
"to the system."
msgstr ""

#: models.py:204 models.py:1058
msgid "Added"
msgstr "Aggiunto"

#: models.py:208
msgid "The dominant language in the document."
msgstr ""

#: models.py:213
msgid "Whether or not this document is in the trash."
msgstr ""

#: models.py:214
msgid "In trash?"
msgstr "Nel cestino?"

#: models.py:219
msgid "The server date and time when the document was moved to the trash."
msgstr ""

#: models.py:221
msgid "Date and time trashed"
msgstr "Data e ora di spostamento nel cestino"

#: models.py:225
msgid ""
"A document stub is a document with an entry on the database but no file "
"uploaded. This could be an interrupted upload or a deferred upload via the "
"API."
msgstr "Un documento troncato è un documento presente sul database che non ha un file scaricato. Potrebbe essere dovuto ad un upload interrotto oppure rimandato per caricarlo via API."

#: models.py:228
msgid "Is stub?"
msgstr "Documento troncato?"

#: models.py:241
#, python-format
msgid "Document stub, id: %d"
msgstr "Documento troncato, ID: %d"

#: models.py:431
msgid "The server date and time when the document version was processed."
msgstr ""

#: models.py:432
msgid "Timestamp"
msgstr "Timestamp"

#: models.py:436
msgid "An optional short text describing the document version."
msgstr ""

#: models.py:437
msgid "Comment"
msgstr "Commento"

#: models.py:443
msgid "File"
msgstr "File"

#: models.py:447
msgid ""
"The document version's file mimetype. MIME types are a standard way to "
"describe the format of a file, in this case the file format of the document."
" Some examples: \"text/plain\" or \"image/jpeg\". "
msgstr ""

#: models.py:451 search.py:19 search.py:39
msgid "MIME type"
msgstr "Tipo MIME"

#: models.py:455
msgid ""
"The document version file encoding. binary 7-bit, binary 8-bit, text, "
"base64, etc."
msgstr ""

#: models.py:457
msgid "Encoding"
msgstr "Codifica"

#: models.py:469 models.py:470 models.py:788
msgid "Document version"
msgstr "Versione documento"

#: models.py:774
msgid "Quick label"
msgstr "Etichetta rapida"

#: models.py:792
msgid "Page number"
msgstr "Numero di pagina"

#: models.py:799 models.py:1011 models.py:1044
msgid "Document page"
msgstr "Pagina del documento"

#: models.py:800 models.py:1045
msgid "Document pages"
msgstr "Le pagine del documento"

#: models.py:804
#, python-format
msgid "Page %(page_num)d out of %(total_pages)d of %(document)s"
msgstr "Pagina %(page_num)d di %(total_pages)d del %(document)s"

#: models.py:1014
msgid "Date time"
msgstr "Data e ora"

#: models.py:1016
msgid "Filename"
msgstr "Nome file"

#: models.py:1024
msgid "Document page cached image"
msgstr "Immagini pagine documenti in cache"

#: models.py:1025
msgid "Document page cached images"
msgstr "Immagini pagine documenti in cache"

#: models.py:1064
msgid "Duplicated document"
msgstr "documento duplicato"

#: models.py:1078 models.py:1107
msgid "User"
msgstr "Utente"

#: models.py:1088
msgid "Favorite document"
msgstr ""

#: models.py:1089
msgid "Favorite documents"
msgstr ""

#: models.py:1114
msgid "Accessed"
msgstr "Acceduto"

#: models.py:1121
msgid "Recent document"
msgstr "Documento recente "

#: models.py:1122
msgid "Recent documents"
msgstr "Documenti recenti"

#: permissions.py:10
msgid "Create documents"
msgstr "Crea documenti"

#: permissions.py:13
msgid "Delete documents"
msgstr "Cancella documenti"

#: permissions.py:16
msgid "Trash documents"
msgstr "Cestina documenti"

#: permissions.py:19 views/document_views.py:502
msgid "Download documents"
msgstr "Scarica documenti"

#: permissions.py:22
msgid "Edit documents"
msgstr "Modifica documenti"

#: permissions.py:25
msgid "Create new document versions"
msgstr "Creazione di nuove versioni dei documenti"

#: permissions.py:28
msgid "Edit document properties"
msgstr "Modifica proprietà documento"

#: permissions.py:31
msgid "Print documents"
msgstr "Stampa documenti"

#: permissions.py:34
msgid "Restore trashed document"
msgstr "Ripristina il documento cancellato"

#: permissions.py:37
msgid "Execute document modifying tools"
msgstr "Esegui i tools per la modifica dei documenti"

#: permissions.py:41
msgid "Revert documents to a previous version"
msgstr "Ripristinare i documenti ad una versione precedente"

#: permissions.py:45
msgid "View documents' versions list"
msgstr ""

#: permissions.py:48
msgid "View documents"
msgstr "Visualizza documenti"

#: permissions.py:58
msgid "Create document types"
msgstr "Crea tipo di documento"

#: permissions.py:61
msgid "Delete document types"
msgstr "Cancella il tipo di documento"

#: permissions.py:64
msgid "Edit document types"
msgstr "Modifica il tipo di documento"

#: permissions.py:67
msgid "View document types"
msgstr "Visualizza i tipi di documento"

#: queues.py:9
msgid "Converter"
msgstr "Convertitore"

#: queues.py:12
msgid "Documents periodic"
msgstr ""

#: queues.py:15
msgid "Uploads"
msgstr "Caricamenti"

#: queues.py:23
msgid "Generate document page image"
msgstr ""

#: queues.py:28
msgid "Delete a document"
msgstr "Cancella documento"

#: queues.py:32
msgid "Clean empty duplicate lists"
msgstr ""

#: queues.py:37
msgid "Check document type delete periods"
msgstr ""

#: queues.py:41
msgid "Check document type trash periods"
msgstr ""

#: queues.py:45
msgid "Delete document stubs"
msgstr "Cancella documenti incompleti"

#: queues.py:50
msgid "Clear image cache"
msgstr "Pulisci la cache immagini"

#: queues.py:55
msgid "Update document page count"
msgstr ""

#: queues.py:59
msgid "Upload new document version"
msgstr ""

#: settings.py:17
msgid ""
"Path to the Storage subclass to use when storing the cached document image "
"files."
msgstr ""

#: settings.py:26
msgid "Arguments to pass to the DOCUMENT_CACHE_STORAGE_BACKEND."
msgstr ""

#: settings.py:32
msgid ""
"Disables the first cache tier which stores high resolution, non transformed "
"versions of documents's pages."
msgstr ""

#: settings.py:39
msgid ""
"Disables the second cache tier which stores medium to low resolution, "
"transformed (rotated, zoomed, etc) versions of documents' pages."
msgstr ""

#: settings.py:53
msgid "Maximum number of favorite documents to remember per user."
msgstr ""

#: settings.py:59
msgid ""
"Detect the orientation of each of the document's pages and create a "
"corresponding rotation transformation to display it rightside up. This is an"
" experimental feature and it is disabled by default."
msgstr ""

#: settings.py:67
msgid "Default documents language (in ISO639-3 format)."
msgstr ""

#: settings.py:71
msgid "List of supported document languages. In ISO639-3 format."
msgstr ""

#: settings.py:76
msgid ""
"Time in seconds that the browser should cache the supplied document images. "
"The default of 31559626 seconds corresponde to 1 year."
msgstr ""

#: settings.py:95
msgid ""
"Maximum number of recently accessed (created, edited, viewed) documents to "
"remember per user."
msgstr ""

#: settings.py:102
msgid "Maximum number of recently created documents to show."
msgstr ""

#: settings.py:108
msgid "Amount in degrees to rotate a document page per user interaction."
msgstr "Quantità di gradi per la rotazione della pagina del documento"

#: settings.py:114
msgid "Path to the Storage subclass to use when storing document files."
msgstr ""

#: settings.py:122
msgid "Arguments to pass to the DOCUMENT_STORAGE_BACKEND."
msgstr ""

#: settings.py:126
msgid "Height in pixels of the document thumbnail image."
msgstr ""

#: settings.py:137
msgid ""
"Maximum amount in percent (%) to allow user to zoom in a document page "
"interactively."
msgstr "Ingrandimento massimo in percentuale (%) da consentire all'utente per una pagina del documento in modo interattivo."

#: settings.py:144
msgid ""
"Minimum amount in percent (%) to allow user to zoom out a document page "
"interactively."
msgstr "Quantità minima in percentuale (%) per consentire all'utente di ingrandire una pagina di documento in modo interattivo."

#: settings.py:151
msgid "Amount in percent zoom in or out a document page per user interaction."
msgstr "Percentuale dello zoom di una pagina del documento per l'interazione dell'utente."

#: statistics.py:16
msgid "January"
msgstr ""

#: statistics.py:16
msgid "February"
msgstr ""

#: statistics.py:16
msgid "March"
msgstr ""

#: statistics.py:16
msgid "April"
msgstr ""

#: statistics.py:16
msgid "May"
msgstr ""

#: statistics.py:17
msgid "June"
msgstr ""

#: statistics.py:17
msgid "July"
msgstr ""

#: statistics.py:17
msgid "August"
msgstr ""

#: statistics.py:17
msgid "September"
msgstr ""

#: statistics.py:17
msgid "October"
msgstr ""

#: statistics.py:18
msgid "November"
msgstr ""

#: statistics.py:18
msgid "December"
msgstr ""

#: statistics.py:235
msgid "New documents per month"
msgstr "Nuovi documenti per mese"

#: statistics.py:242
msgid "New document versions per month"
msgstr "Nuove versioni dei documenti per mese"

#: statistics.py:249
msgid "New document pages per month"
msgstr "Nuove pagine di documento per mese"

#: statistics.py:256
msgid "Total documents at each month"
msgstr "Totale documenti per ogni mese"

#: statistics.py:263
msgid "Total document versions at each month"
msgstr "Totale versioni di documento documento per ogni mese"

#: statistics.py:270
msgid "Total document pages at each month"
msgstr "Totale pagine documento per ogni mese"

#: templates/documents/forms/widgets/document_page_carousel.html:16
#, python-format
msgid ""
"\n"
"                    Page %(page_number)s of %(total_pages)s\n"
"                "
msgstr ""

#: templates/documents/forms/widgets/document_page_carousel.html:22
msgid "No pages to display"
msgstr ""

#: views/document_page_views.py:49
#, python-format
msgid "Pages for document: %s"
msgstr "Pagine del documento: %s"

#: views/document_page_views.py:104
msgid "Unknown view keyword argument schema, unable to redirect."
msgstr ""

#: views/document_page_views.py:136
msgid "There are no more pages in this document"
msgstr "Non ci sono più pagine in questo documento"

#: views/document_page_views.py:153
msgid "You are already at the first page of this document"
msgstr "Sei già alla prima pagina del documento"

#: views/document_page_views.py:181
#, python-format
msgid "Image of: %s"
msgstr "Immagine di: %s"

#: views/document_type_views.py:46
#, python-format
msgid "Documents of type: %s"
msgstr "Documento di tipo: %s"

#: views/document_type_views.py:64
msgid ""
"Document types are the most basic units of configuration. Everything in the "
"system will depend on them. Define a document type for each type of physical"
" document you intend to upload. Example document types: invoice, receipt, "
"manual, prescription, balance sheet."
msgstr ""

#: views/document_type_views.py:70
msgid "No document types available"
msgstr ""

#: views/document_type_views.py:102
msgid "All documents of this type will be deleted too."
msgstr "Tutti i documenti di questo tipo saranno cancellati ."

#: views/document_type_views.py:104
#, python-format
msgid "Delete the document type: %s?"
msgstr "Cancellare il tipo documento: %s?"

#: views/document_type_views.py:120
#, python-format
msgid "Edit document type: %s"
msgstr "Modifica il tipo di documento: %s"

#: views/document_type_views.py:150
#, python-format
msgid "Create quick label for document type: %s"
msgstr "Crea etichetta rapida per il tipo documento: %s"

#: views/document_type_views.py:171
#, python-format
msgid "Edit quick label \"%(filename)s\" from document type \"%(document_type)s\""
msgstr "Modifica etichetta rapida \"%(filename)s\" dal tipo documento \"%(document_type)s\""

#: views/document_type_views.py:196
#, python-format
msgid ""
"Delete the quick label: %(label)s, from document type \"%(document_type)s\"?"
msgstr "Cancellare l'etichetta: %(label)s, dal tipo documento \"%(document_type)s\"?"

#: views/document_type_views.py:232
msgid ""
"Quick labels are predetermined filenames that allow the quick renaming of "
"documents as they are uploaded by selecting them from a list. Quick labels "
"can also be used after the documents have been uploaded."
msgstr ""

#: views/document_type_views.py:238
msgid "There are no quick labels for this document type"
msgstr ""

#: views/document_type_views.py:241
#, python-format
msgid "Quick labels for document type: %s"
msgstr "Etichette rapide per il tipo documento: %s"

#: views/document_version_views.py:48
#, python-format
msgid "Versions of document: %s"
msgstr "Versione del documento: %s"

#: views/document_version_views.py:62
msgid "All later version after this one will be deleted too."
msgstr "Tutte le versioni precedenti a questa verranno cancellate."

#: views/document_version_views.py:65
msgid "Revert to this version?"
msgstr "Ripristinare questa versione?"

#: views/document_version_views.py:75
msgid "Document version reverted successfully"
msgstr "Versione del documento ripristinato con successo"

#: views/document_version_views.py:80
#, python-format
msgid "Error reverting document version; %s"
msgstr "Errore restituito, al ripristino del documento; %s"

#: views/document_version_views.py:99
msgid "Download document version"
msgstr ""

#: views/document_version_views.py:165
#, python-format
msgid "Preview of document version: %s"
msgstr ""

#: views/document_views.py:71
#, python-format
msgid "Error retrieving document list: %(exception)s."
msgstr ""

#: views/document_views.py:91
msgid ""
"This could mean that no documents have been uploaded or that your user "
"account has not been granted the view permission for any document or "
"document type."
msgstr ""

#: views/document_views.py:95
msgid "No documents available"
msgstr ""

#: views/document_views.py:109
msgid "Delete the selected document?"
msgstr "Cancellare il documento selezionato?"

#: views/document_views.py:130
#, python-format
msgid "Document: %(document)s deleted."
msgstr "Documento: %(document)s cancellato."

#: views/document_views.py:138
msgid "Delete the selected documents?"
msgstr "Cancellare i documenti selezionati?"

#: views/document_views.py:161
msgid ""
"To avoid loss of data, documents are not deleted instantly. First, they are "
"placed in the trash can. From here they can be then finally deleted or "
"restored."
msgstr ""

#: views/document_views.py:166
msgid "There are no documents in the trash can"
msgstr ""

#: views/document_views.py:179
#, python-format
msgid "Document type change request performed on %(count)d document"
msgstr ""

#: views/document_views.py:182
#, python-format
msgid "Document type change request performed on %(count)d documents"
msgstr ""

#: views/document_views.py:189
msgid "Change"
msgstr "Modifica"

#: views/document_views.py:191
msgid "Change the type of the selected document"
msgid_plural "Change the type of the selected documents"
msgstr[0] ""
msgstr[1] ""

#: views/document_views.py:202
#, python-format
msgid "Change the type of the document: %s"
msgstr "Cambia il tipo di documento: %s"

#: views/document_views.py:223
#, python-format
msgid "Document type for \"%s\" changed successfully."
msgstr "Tipo documento per \"%s\" cambiato con successo."

#: views/document_views.py:248
msgid "Only exact copies of this document will be shown in the this list."
msgstr ""

#: views/document_views.py:252
msgid "There are no duplicates for this document"
msgstr ""

#: views/document_views.py:255
#, python-format
msgid "Duplicates for document: %s"
msgstr "Duplicati per il documento: %s"

#: views/document_views.py:284
#, python-format
msgid "Edit properties of document: %s"
msgstr "Modifica le proprietà del documento: %s"

#: views/document_views.py:318
#, python-format
msgid "Preview of document: %s"
msgstr "Anteprima del documento: %s"

#: views/document_views.py:324
msgid "Restore the selected document?"
msgstr "Ripristinare i documenti selezionati?"

#: views/document_views.py:345
#, python-format
msgid "Document: %(document)s restored."
msgstr "Documento: %(document)s rispristinato."

#: views/document_views.py:353
msgid "Restore the selected documents?"
msgstr "Ripristinare i documenti selezionati?"

#: views/document_views.py:364
#, python-format
msgid "Move \"%s\" to the trash?"
msgstr "Spostare \"%s\" nel cestino?"

#: views/document_views.py:387
#, python-format
msgid "Document: %(document)s moved to trash successfully."
msgstr "Documento: %(document)s spostato nel cestino con successo.."

#: views/document_views.py:400
msgid "Move the selected documents to the trash?"
msgstr "Spostare i documenti selezionati nel cestino?"

#: views/document_views.py:418
#, python-format
msgid "Properties for document: %s"
msgstr "Proprietà del documento: %s"

#: views/document_views.py:424
msgid "Empty trash?"
msgstr "Cancellare il cestino?"

#: views/document_views.py:437
msgid "Trash emptied successfully"
msgstr "Svuotamento cestino completato"

#: views/document_views.py:500
msgid "Download"
msgstr "Scarica"

#: views/document_views.py:606
#, python-format
msgid "%(count)d document queued for page count recalculation"
msgstr ""

#: views/document_views.py:609
#, python-format
msgid "%(count)d documents queued for page count recalculation"
msgstr ""

#: views/document_views.py:617
msgid "Recalculate the page count of the selected document?"
msgid_plural "Recalculate the page count of the selected documents?"
msgstr[0] "Ricalcolare il conteggio delle pagine del documento selezionato?"
msgstr[1] "Ricalcolare il conteggio delle pagine dei documenti selezionati?"

#: views/document_views.py:628
#, python-format
msgid "Recalculate the page count of the document: %s?"
msgstr ""

#: views/document_views.py:644
#, python-format
msgid ""
"Document \"%(document)s\" is empty. Upload at least one document version "
"before attempting to detect the page count."
msgstr ""

#: views/document_views.py:657
#, python-format
msgid "Transformation clear request processed for %(count)d document"
msgstr ""

#: views/document_views.py:660
#, python-format
msgid "Transformation clear request processed for %(count)d documents"
msgstr ""

#: views/document_views.py:668
msgid "Clear all the page transformations for the selected document?"
msgid_plural "Clear all the page transformations for the selected document?"
msgstr[0] ""
msgstr[1] ""

#: views/document_views.py:679
#, python-format
msgid "Clear all the page transformations for the document: %s?"
msgstr ""

#: views/document_views.py:694 views/document_views.py:722
#, python-format
msgid ""
"Error deleting the page transformations for document: %(document)s; "
"%(error)s."
msgstr "Errore nella cancellazione della trasformazione della pagina per il documento:%(document)s; %(error)s."

#: views/document_views.py:730
msgid "Transformations cloned successfully."
msgstr "Clonazione delle trasformazioni completata."

#: views/document_views.py:745 views/document_views.py:818
msgid "Submit"
msgstr "Invia"

#: views/document_views.py:747
#, python-format
msgid "Clone page transformations for document: %s"
msgstr ""

#: views/document_views.py:821
#, python-format
msgid "Print: %s"
msgstr "Stampa: %s"

#: views/document_views.py:856
msgid ""
"Duplicates are documents that are composed of the exact same file, down to "
"the last byte. Files that have the same text or OCR but are not identical or"
" were saved using a different file format will not appear as duplicates."
msgstr ""

#: views/document_views.py:863
msgid "There are no duplicated documents"
msgstr ""

#: views/document_views.py:881
#, python-format
msgid ""
"Favorited documents will be listed in this view. Up to %(count)d documents "
"can be favorited per user. "
msgstr ""

#: views/document_views.py:884
msgid "There are no favorited documents."
msgstr ""

#: views/document_views.py:895
#, python-format
msgid "%(count)d document added to favorites."
msgstr ""

#: views/document_views.py:898
#, python-format
msgid "%(count)d documents added to favorites."
msgstr ""

#: views/document_views.py:905
msgid "Add"
msgstr "Aggiungi"

#: views/document_views.py:908
msgid "Add the selected document to favorites"
msgid_plural "Add the selected documents to favorites"
msgstr[0] ""
msgstr[1] ""

#: views/document_views.py:921
#, python-format
msgid "Document \"%(instance)s\" is not in favorites."
msgstr ""

#: views/document_views.py:925
#, python-format
msgid "%(count)d document removed from favorites."
msgstr ""

#: views/document_views.py:928
#, python-format
msgid "%(count)d documents removed from favorites."
msgstr ""

#: views/document_views.py:935
msgid "Remove"
msgstr "Rimuovi"

#: views/document_views.py:938
msgid "Remove the selected document from favorites"
msgid_plural "Remove the selected documents from favorites"
msgstr[0] ""
msgstr[1] ""

#: views/document_views.py:963
msgid ""
"This view will list the latest documents viewed or manipulated in any way by"
" this user account."
msgstr ""

#: views/document_views.py:967
msgid "There are no recently accessed document"
msgstr ""

#: views/document_views.py:987
msgid "This view will list the latest documents uploaded in the system."
msgstr ""

#: views/document_views.py:991
msgid "There are no recently added document"
msgstr ""

#: views/misc_views.py:18
msgid "Clear the document image cache?"
msgstr "Cancellare la cache delle immagini documento?"

#: views/misc_views.py:25
msgid "Document cache clearing queued successfully."
msgstr "Pulizia della cache documenti in coda completata con successo."

#: views/misc_views.py:31
msgid "Scan for duplicated documents?"
msgstr ""

#: views/misc_views.py:38
msgid "Duplicated document scan queued successfully."
msgstr ""

#: widgets.py:81 widgets.py:85
#, python-format
msgid "Pages: %d"
msgstr ""
